﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using tech_test.Payment.API.Models;

namespace tech_test.Payments.Domain.Sales
{
    public interface ISaleRepository
    {
        Task<Sale> ObterPorId(Guid id);
        Task Adicionar(Sale venda);
        void AtualizarStatus(PaymentStatus status, Guid id);
        Task<int> GetCount();

    }
}
