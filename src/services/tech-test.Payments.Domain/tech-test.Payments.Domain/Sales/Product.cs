﻿using System.Collections;
using System.Collections.Generic;

namespace tech_test.Payment.API.Models
{
    public class Product : EntidadeBase
    {
        public string Nome { get; set; }
        public int Codigo { get; set; }
        public double Valor { get; set; }
    }
}
