﻿using System.Collections;
using System.Collections.Generic;

namespace tech_test.Payment.API.Models
{
    public class Seller : EntidadeBase
    {
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
