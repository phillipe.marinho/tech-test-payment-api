﻿namespace tech_test.Payment.API.Models
{
    public enum PaymentStatus
    {
        Aguadando_Pagamento,
        Pagamento_Aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelado
    }
}
