﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test.Payment.API.Models
{
    public class Sale : EntidadeBase
    {

        public int NumeroPedido { get; set; }

        [ForeignKey("Seller")]
        public int VendedorId { get; set; }
        public virtual Seller Vendedor { get; set; }

        [ForeignKey("Product")]
        public int ProdutoId { get; set; }
        public virtual Product Produto { get; set; }
        public DateTime Data { get; set; }
        public PaymentStatus Status { get; set; }


        public void addSequenciaNumeroPedido(int numeroPedido)
        {
            NumeroPedido = numeroPedido;
        }
    }


}
