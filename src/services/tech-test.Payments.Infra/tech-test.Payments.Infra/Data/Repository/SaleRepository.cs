﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using tech_test.Payment.API.Models;
using tech_test.Payments.Domain.Sales;

namespace tech_test.Payments.Infra.Data.Repository
{
    public class SaleRepository : ISaleRepository
    {
        private readonly PaymentsContext _context;

        public SaleRepository(PaymentsContext context)
        {
            _context = context;
        }
        public async Task Adicionar(Sale venda)
        {
           await _context.Sales.AddAsync(venda);
            await _context.SaveChangesAsync();
        }

        public void AtualizarStatus(PaymentStatus status, Guid id)
        {

            var pedidoBD = _context.Sales.Find(id);

            if (pedidoBD.Status == PaymentStatus.Aguadando_Pagamento)
            {
                if (status == PaymentStatus.Pagamento_Aprovado)
                {
                    pedidoBD.Status = PaymentStatus.Pagamento_Aprovado;
                }
                else if (status == PaymentStatus.Cancelado)
                {
                    pedidoBD.Status = PaymentStatus.Cancelado;
                }

            }
            else if (pedidoBD.Status == PaymentStatus.Pagamento_Aprovado)
            {
                if (status == PaymentStatus.Enviado_para_transportadora)
                {
                    pedidoBD.Status = PaymentStatus.Enviado_para_transportadora;
                }
                else if (status == PaymentStatus.Cancelado)
                {
                    pedidoBD.Status = PaymentStatus.Cancelado;
                }

            }
            else if (pedidoBD.Status == PaymentStatus.Enviado_para_transportadora)
            {
                if (status == PaymentStatus.Entregue)
                {
                    pedidoBD.Status = PaymentStatus.Entregue;
                }

            }
            else
            {
                
            }

            _context.Sales.Update(pedidoBD);
            _context.SaveChanges();

        }

        public async Task<Sale> ObterPorId(Guid id)
        {
            var venda = await _context.Sales.FindAsync(id);


            return venda;
        }




        public async Task<int> GetCount()
        {
            try
            {
                return await _context.Sales.CountAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
