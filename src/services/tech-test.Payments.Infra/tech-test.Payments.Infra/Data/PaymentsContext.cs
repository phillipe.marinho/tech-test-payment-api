﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using tech_test.Payment.API.Models;

namespace tech_test.Payments.Infra.Data
{
    public class PaymentsContext : DbContext
    {
            public PaymentsContext(DbContextOptions<PaymentsContext> options) : base(options) { }

            public DbSet<Sale> Sales { get; set; }
            public DbSet<Seller> Sellers { get; set; }
            public DbSet<Product> Products { get; set; }

    }
}
