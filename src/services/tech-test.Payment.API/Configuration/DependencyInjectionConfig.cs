﻿using Microsoft.Extensions.DependencyInjection;
using tech_test.Payments.Domain.Sales;
using tech_test.Payments.Infra.Data.Repository;

namespace tech_test.Payment.API.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static void RegisterServices(this IServiceCollection services)
        {
          

            // Data
            services.AddScoped<ISaleRepository, SaleRepository>();

        }
    }
}
