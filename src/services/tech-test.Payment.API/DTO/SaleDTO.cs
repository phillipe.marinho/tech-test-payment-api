﻿using System;
using System.Collections.Generic;
using tech_test.Payment.API.Models;

namespace tech_test.Payment.API.DTO
{
    public class SaleDTO
    {
        public int Status { get; set; }
        public Seller Vendedor { get; set; }
        public DateTime Data { get; set; }
        public IEnumerable<Product> Produtos { get; set; }
    }
}
