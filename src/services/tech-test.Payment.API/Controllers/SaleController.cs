﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using tech_test.Payment.API.DTO;
using tech_test.Payment.API.Models;
using tech_test.Payments.Domain.Sales;

namespace tech_test.Payment.API.Controllers
{
    public class SaleController : MainController
    {

        private readonly ISaleRepository _saleRepository;

        public SaleController(ISaleRepository saleRepository)
        {
            _saleRepository = saleRepository;
        }

        [HttpPost("registrar-venda")]
        public async Task<IActionResult> AdicionarVenda(Sale venda)
        {
            if (!ModelState.IsValid) return View(venda);

            if(venda.Produto == null)
            {
                return View(venda);
            }

            var pedido = await CriarPedido(venda);

            await _saleRepository.Adicionar(pedido);

            return CustomResponse();
        }


        [HttpGet("buscar-venda/{id}")]
        public async Task<Sale> BuscarVenda(Guid id)
        {
            return await _saleRepository.ObterPorId(id);
        }

        [HttpPut("atualizar-pagamento")]
        public IActionResult AtualizarPagamento(PaymentStatus status, Guid id)
        {
             _saleRepository.AtualizarStatus(status, id);
            return CustomResponse();
        }


        private async Task<Sale> CriarPedido(Sale venda)
        {
            var pedido = new Sale
            {
               Id = Guid.NewGuid(),
               NumeroPedido = await _saleRepository.GetCount() +1,
               Data = DateTime.Now,
               Status = PaymentStatus.Aguadando_Pagamento,
               Vendedor = venda.Vendedor,
               Produto = venda.Produto,
               VendedorId = 10,
               ProdutoId = 1
            };


            return pedido;
        }
    }
}
